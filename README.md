# Equipe de Documentação do Projeto

## Objetivos:

- Realizar a documentação do projeto TetrinetJS.

## Grupo 3 - Membros

- Luana Silva Carvalho
- Debora dos Santos e Silva
- Carine Sá de Morais Aquino
- Gustavo Alves de Souza


## Tarefas do Sprint 1 (02/09/19 - 08/09/19)

- [Criar documentação do server atual](https://gitlab.com/tetrinetjs/server)
- [Revisar histórias de usuários](https://en.wikipedia.org/wiki/User_story)
- [Revisar diagrama de sequência](https://gitlab.com/tetrinetjs/devel-docs/tree/master/Pages)
- [Revisar documentação tretinetProtocol4](https://github.com/xale/iTetrinet/wiki/tetrinet-protocol:-server-to-client-messages)

## Tarefas do Sprint 2 (09/09/19 - 15/09/19)



## Tarefas do Sprint 3 (16/09/19 - 22/09/19)



## Tarefas do Sprint 4 (23/09/19 - 29/09/19)